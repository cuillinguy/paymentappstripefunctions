import { AzureFunction, Context, HttpRequest } from "@azure/functions"

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function processed a request.');
    const name = (req.query.name || (req.body && req.body.name));
    const responseMessage = name
        ? "Hello, " + name + ". This HTTP triggered function executed successfully. Oh Yes"
        : "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response. Three";

        const stripe = require('stripe')('sk_test_a7TyTZ0PpsYvFAf5tEW5091T00UCPZigm4');// Secret key

        

        // Token is created using Stripe Checkout or Elements!
        // Get the payment token ID submitted by the form:
        // const token = req.body.stripeToken; // Using Express

        console.log('body ' + req.body.stripeToken)

        // console.log('token ' + token)
        
         const charge = await stripe.charges.create({
           amount: 150.00,
           currency: 'usd',
           description: 'Example charge',
           source: 'tok_1HoU9EFMDlQAsNe81XhcHCoi',
         });

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: responseMessage + ' ' + req.body.stripeToken
    };

};

export default httpTrigger;